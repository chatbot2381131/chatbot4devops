# Set the base image to use. In this case, we use the official Python image.
FROM python:3.9

# Set the working directory inside the container. This is where the app will be copied.
WORKDIR /app

# Copy the requirements.txt file to the container.
COPY requirements.txt .

# Install the Python dependencies.
RUN pip install --no-cache-dir -r requirements.txt

# Copy the rest of the application files to the container.
COPY . .

# Expose the port your Flask app will run on. Make sure it matches the port in your Flask app.
EXPOSE 5000

# Define the command to start your Flask application.
CMD ["python", "main.py"]
